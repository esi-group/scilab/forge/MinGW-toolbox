// Copyright (C) DIGITEO - 2010 - Allan CORNET
// Copyright (C) Scilab Enterprises - 2012 - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//=============================================================================
function Makename = dlwGenerateMakefile(name, ..
                                 tables, ..
                                 files, ..
                                 libs, ..
                                 libname, ..
                                 with_gateway, ..
                                 ldflags, ..
                                 cflags, ..
                                 fflags, ..
                                 cc)

  [lhs,rhs] = argn(0);
  if rhs < 4 then
    error(msprintf(gettext("%s: Wrong number of input argument(s).\n"), "dlwGenerateMakefile"));
    return
  end

  if argn(2) < 6 then
    with_gateway = %t;
    ldflags = '';
    cflags = '';
    fflags = '';
    cc = '';
  end

  if ~isdef('makename') then
    makename = '';
  end

  for i=1:size(files,'*') // compatibility scilab 4.x
    [path_f, file_f, ext_f] = fileparts(files(i));
    if or(ext_f == ['.o','.obj']) then
      files(i) = path_f + file_f;
    else
      files(i) = path_f + file_f + ext_f;
    end
  end

  // change table if necessary
  if tables <> [] then
    if typeof(tables)<>'list' then
      tables = list(tables)
    end
    L = length(tables);

    for it = 1:L
      table = tables(it);
      [mt, nt] = size(table);

      if nt == 2 then
        col= "csci";
        table = [table, col(ones(mt,1))];
        nt=3;
      end

      if nt<>3 then
        error(msprintf(gettext("%s: Wrong size for input argument #%d.\n") ,"dlwGenerateMakefile", 2));
      end
      tables(it) = table;
    end
  end

  if isempty(makename) then
    Makename = dlwGetDefltMakefileName() + dlwGetMakefileExt();
  else
    Makename = makename + dlwGetMakefileExt();
  end

  if length(libname) > 0  & strncpy(libname, 3) <> 'lib' then 
    libname = 'lib' + libname;
  end

  ilib_gen_Make_mingw(name, tables, files, libs, libname, Makename, with_gateway, ldflags, cflags, fflags)

endfunction
//=============================================================================
function ilib_gen_Make_mingw(name, tables, files, libs, libname, Makename, with_gateway, ldflags, cflags, fflags)
  managed_ext = [".cxx", ".cpp", ".c", ".f90", ".f"];
  obj_ext = ['.o', '.obj', ''];
  
  GCCLIBDIR = """" + mgw_getLibPath() + """";
  SCIDIR = SCI;
  SCIDIR1= pathconvert(SCI,%f,%f,'w');
  SCIMINGWLIBDIR = mgw_getScilabLibPath();
  CFLAGS = cflags;
  MEXCFLAGS = '';
  FFLAGS = fflags;
  MEXFFLAGS = '';
  LDFLAGS = ldflags;
  FILES_SRC = '';
  OBJS = '';
  OTHERLIBS = '';

  if isempty(libname) then
    LIBRARY = name; 
  else
    LIBRARY = libname; 
  end
  
  FILES_SRC_MATRIX = [];
  
  [path_Make, file_Make, ext_Make] = fileparts(Makename);
  
    for i=1:size(files,"*")
        [path_f, file_f, ext_f] = fileparts(files(i));
        FILENAME = [];
        FILE_FOUND = %f;
        for y = managed_ext(:)'
            if (FILE_FOUND == %f) then
                if (fileinfo(path_f + file_f + y) <> []) | (fileinfo(path_Make + file_f + y) <> []) then
                    FILENAME = path_f + file_f + y;
                    FILE_FOUND = %t;
                end
            end
        end
        
        FILES_SRC_MATRIX = [FILES_SRC_MATRIX , FILENAME];
    end

  if typeof(tables) <> 'list' then 
    tables = list(tables);
  end
  L = length(tables); 

  if with_gateway then
    if L == 1 then
        FILES_SRC_MATRIX = [FILES_SRC_MATRIX , name + ".cpp"];
      else
        for i=1:L
            FILES_SRC_MATRIX = [FILES_SRC_MATRIX , name + string(i) + ".cpp"];
        end
    end
  end
  
  for it=1:L
    table = tables(it);
    [mt,nt] = size(table);

    for i=1:mt
      if table(i,3)=="cmex" | table(i,3)=="fmex" | table(i,3)=="Fmex" then
        MEXCFLAGS = "-Dmexfunction_=mex" + table(i,2) + "_ -DmexFunction=mex_" + table(i,2);
        MEXFFLAGS = "-Dmexfunction=mex" + table(i,2);
        if table(i,3)=="cmex" then
          if find(listfiles() == table(i,2)+".cpp") then
            filenameMex = table(i,2) + ".cpp";
          else
            filenameMex = table(i,2) + ".c";
          end
        else
          filenameMex = table(i,2) + ".f";
        end
        if grep(FILES_SRC_MATRIX,filenameMex) == [] then
          FILES_SRC_MATRIX = [FILES_SRC_MATRIX , filenameMex];
        end
      end
    end
  end
  
  if isempty(FILES_SRC_MATRIX) | ~and(isfile(FILES_SRC_MATRIX)) then
    error(999, msprintf(_("%s: Wrong value for input argument #%d: existing file(s) expected.\n"), "ilib_gen_Make", 3));
  end
  
  //update DEBUG_SCILAB_DYNAMIC_LINK to map with Scilab compilation mode
  val = getenv("DEBUG_SCILAB_DYNAMIC_LINK","");
  if val <> "YES" & val <> "NO" & isDebug() then
    setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES");
    CFLAGS = CFLAGS + " -D_DEBUG";
  else
    setenv("DEBUG_SCILAB_DYNAMIC_LINK","");
    CFLAGS = CFLAGS + " -DNDEBUG";
  end
  
  // remove duplicate files
  FILES_SRC_MATRIX = unique(FILES_SRC_MATRIX,"keepOrder");
  FILES_SRC = strcat(FILES_SRC_MATRIX,' ');

  OBJS_MATRIX = [];
  
  for y = 1:size(FILES_SRC_MATRIX,'*')
    [path_f, file_f, ext_f] = fileparts(FILES_SRC_MATRIX(y));
    OBJS_MATRIX = [OBJS_MATRIX, path_f + file_f + '.o'];
  end
  
  OBJS = strcat(OBJS_MATRIX, ' ');
  

  for x=libs(:)'
     if (x <> [] & x <> '') then 
       if OTHERLIBS <> '' then
         OTHERLIBS = OTHERLIBS + ' ' + x + '.a';
       else
         OTHERLIBS = x + '.a';
       end
     end
  end
  
  OTHERLIBS = strsubst(OTHERLIBS,'/',filesep());

  scriptfilename = mgw_getRootPath() + filesep() + 'scripts' + filesep() + 'TEMPLATE_MAKEFILE.MINGW';
  
  if isfile(scriptfilename) then
    MAKEFILE_MINGW = mgetl(scriptfilename);
  else
    MAKEFILE_MINGW = '';
    warning(scriptfilename + _('not found.') );
  end
  
  if with_module('scicos') then
    SCICOS_LIBS = "-lscicos -lscicos-cli -lscicos_blocks -lscicos_blocks-cli -lscicos_blocks_f -lscicos_f";
  else
    SCICOS_LIBS = "";  
  end
  
 
  prefix = mgw_getPrefixPkgName();
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__GCC__", prefix  + "-mingw32-gcc.exe");
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__GPP__", prefix  + "-mingw32-g++.exe");
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__FC__", prefix  + "-mingw32-gfortran.exe");
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__LINKER__", prefix  + "-mingw32-g++.exe");
  
  if win64() then  
    MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__ARCH__", "-m64");
    MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__RUNTIME__", "msvcrt");
  else
    MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__ARCH__", "-m32");
    MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__RUNTIME__", "msvcr90");
  end
  
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__GCCLIBDIR__" , GCCLIBDIR);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__SCI__" , SCIDIR);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__XCOS_LIBS__", SCICOS_LIBS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__SCIDIR1__" , SCIDIR1);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__SCIMINGWLIBDIR__" , SCIMINGWLIBDIR);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__CFLAGS__" , CFLAGS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__MEXCFLAGS__" , MEXCFLAGS); 
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__FFLAGS__" , FFLAGS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__MEXFFLAGS__" , MEXFFLAGS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__LDFLAGS__" , LDFLAGS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__LIBNAME__" , LIBRARY);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__FILES_SRC__" , FILES_SRC);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__OBJS__" , OBJS);
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__OTHERSLIBS__" , OTHERLIBS);

  
  SCILAB_INCLUDES = dlwGetScilabIncludes();
  SCILAB_INCLUDES = "-I""" + SCILAB_INCLUDES + """";
  SCILAB_INCLUDES = [SCILAB_INCLUDES(1:$-1) + " \"; SCILAB_INCLUDES($)];
  SCILAB_INCLUDES = strcat(SCILAB_INCLUDES, ascii(10));
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW , "__SCILAB_INCLUDES__" , SCILAB_INCLUDES);

  SCILAB_LIBS = dlwGetScilabLibraries();
  SCILAB_LIBS = "-l" + SCILAB_LIBS + "";
  SCILAB_LIBS = [SCILAB_LIBS(1:$-1) + " \", SCILAB_LIBS($)];
  SCILAB_LIBS = strcat(SCILAB_LIBS, ascii(10));
  MAKEFILE_MINGW = strsubst(MAKEFILE_MINGW, "__SCILAB_LIBS__",SCILAB_LIBS);

  mputl(MAKEFILE_MINGW, Makename);
    
  if ilib_verbose() > 1 then
    disp(mgetl(Makename));
  end
  
endfunction
//---------------------------------------------------------------------------------------
