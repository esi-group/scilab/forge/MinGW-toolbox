// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = mgw_detectCompiler()
  bOK = %F;
  mingwroot = mgw_getMinGwRootPath();
  mingwbin = mgw_getBinPath();
  mingwlib = mgw_getIncludePath();
  mingwinclude = mgw_getLibPath();
  if (mingwroot <> []) & (mingwbin <> []) & (mingwlib <> []) & (mingwinclude <> []) then
    bOK = %T;
  end
endfunction
// =============================================================================
