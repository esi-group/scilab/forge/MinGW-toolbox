// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = mgw_convertLibrary(dllName, destPath, datasSymbols)
  bOK = %f;

  if ~isdef("datasSymbols") then
    datasSymbols = [];
  end

  if isfile(dllName) & isdir(destPath) then
    symbolsList = mgw_getSymbolsDll(dllName);

    // remove symbols only as DATA
    if datasSymbols <> [] then
      nbDatasSymbols = size(datasSymbols, "*");
      datasSymbols = matrix(datasSymbols, nbDatasSymbols, 1);

      for j = 1:nbDatasSymbols
        symbolsList(2)(symbolsList(2) == datasSymbols(j)) = [];
      end
    end

    // remove intermediate intel symbols
    symbolsList(2) = strsubst(symbolsList(2), "/\.$/", "", "r");
    symbolsList(2)(symbolsList(2) == "") = [];

    libraryname = fileparts(dllName, "fname");

    [fdexpw, ferr] = mopen(TMPDIR + filesep() + libraryname + ".exp", "wt");
    if ferr <> 0 then
      return
    end

    mfprintf(fdexpw, "LIBRARY %s%s\n", libraryname, getdynlibext());
    mfprintf(fdexpw,"EXPORTS\n\n");

    if datasSymbols <> [] then
      datasSymbols = datasSymbols + " DATA";
      mputl(datasSymbols, fdexpw);
    end

    symbolsList(2) = "_" + symbolsList(2) + " " + symbolsList(2);
    mputl(symbolsList(2), fdexpw);

    mclose(fdexpw);
    bOK = mgw_buildLib(TMPDIR + filesep() + libraryname + ".exp", destPath);
    if bOK then
      mdelete(TMPDIR + filesep() + libraryname + ".exp");
    end
  end
endfunction
// =============================================================================
