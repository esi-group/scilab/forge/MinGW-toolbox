// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function scilibpath = mgw_getScilabLibPath()
  scilibpath = [];
  if win64() then
    DEFAULT_PATH = SCIHOME + filesep() + 'mingwlib_x64';
  else
    DEFAULT_PATH = SCIHOME + filesep() + 'mingwlib';
  end
  if isdir(DEFAULT_PATH) then
    scilibpath = DEFAULT_PATH;
  else
   if createdir(DEFAULT_PATH) then
     scilibpath = DEFAULT_PATH;
   end
  end
endfunction
// =============================================================================
