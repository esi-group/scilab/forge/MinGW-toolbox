// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function dllinfolist = mgw_getMachineDll(dllname)
  dllinfolist = list();
  if isfile(dllname) then
    [txt, bErr] = dos(""""+ mgw_getArchBinPath() + filesep() + 'objdump"" -x -j .rdata ' + dllname);
    if bErr then
      machinetxt = txt(grep(txt, 'Magic'))
      if grep(machinetxt, 'PE32+') <> [] then 
        machine = 'x64';
      elseif grep(machinetxt, 'PE32') <> [] then
        machine = 'x86';
      end
    end
    dllinfolist(1) = fileparts(dllname, 'fname') + fileparts(dllname, 'extension');
    dllinfolist(2) = machine;
  end
endfunction
