// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = mgw_SetEnv()
  bOK = %F;
  PATH = getenv('PATH', '');
  if PATH <> '' then

    PATH = strsplit(PATH, pathsep());
    PATH = unique(PATH);
    PATH(PATH == '') = [];
    PATH = strcat(PATH, pathsep());
    PATH = mgw_getBinPath() + pathsep() + PATH;
    ierr = setenv('PATH', PATH);
    if ierr <> %T then
      return
    end

    GCCINCLUDE = getenv('GCCINCLUDE', '');
    GCCINCLUDE  = mgw_getIncludePath() + pathsep() + GCCINCLUDE;
    GCCINCLUDE = strsplit(GCCINCLUDE, pathsep());
    GCCINCLUDE = unique(GCCINCLUDE);
    GCCINCLUDE(GCCINCLUDE == '') = [];
    GCCINCLUDE = strcat(GCCINCLUDE, pathsep());
    ierr = setenv('GCCINCLUDE', GCCINCLUDE);
    if ierr <> %T then
      return
    end

    GCCLIB = getenv('GCCLIB', '');
    GCCLIB  = mgw_getLibPath() + pathsep() + GCCLIB;
    GCCLIB = strsplit(GCCLIB, pathsep());
    GCCLIB = unique(GCCLIB);
    GCCLIB(GCCLIB == '') = [];
    GCCLIB = strcat(GCCLIB, pathsep());
    ierr = setenv('GCCLIB', GCCLIB);
    if ierr <> %T then
      return
    end

    EQUATION = mgw_getEquationPkgPath();
    if EQUATION <> [] then
      EQ_LIBRARY_PATH = EQUATION + '\lib';
      ierr = setenv('EQ_LIBRARY_PATH', EQ_LIBRARY_PATH);
      if ierr <> %T then
        return
      end
    end

    bOK = %T;
  end
endfunction
// =============================================================================
