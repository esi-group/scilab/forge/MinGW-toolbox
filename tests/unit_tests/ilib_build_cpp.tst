// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
i=["#include <string>"
    "#include ""api_scilab.h"""
    "extern  ""C"""
    "{"
    "int sci_cppfind(char *fname, void* pvApiCtx);"
    "}"
    "int sci_cppfind(char *fname, void* pvApiCtx)"
    "{"
    "  int *piAddr1 = NULL, *piAddr2 = NULL;"
    "  char *str1 = NULL, *str2 = NULL;"
    "  getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);"
    "  getAllocatedSingleString(pvApiCtx, piAddr1, &str1);"
    "  std::string myMessage(str1);"
    "  freeAllocatedSingleString(str1);"
    ""
    "  getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);"
    "  getAllocatedSingleString(pvApiCtx, piAddr2, &str2);"
    "  std::string search(str2);"
    "  freeAllocatedSingleString(str2);"
    "  double position = 0;"
    "  if (myMessage.find(search) != std::string::npos) {"
    "    position = myMessage.find(search); /* The actual operation */"
    "  } else {"
    "    position = -1; /* Substring not found */"
    "  }"
    ""
    "  createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, position);"
    "  AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;"
    "  return 0;"
    "}"];

cd TMPDIR;
mputl(i, "sci_cppfind.cxx");

//creating the shared library (a gateway, a Makefile and a loader are 
//generated. 

files=["sci_cppfind.cxx"];
ilib_build("foo",["cppfind","sci_cppfind"],files,[]);

// load the shared library 

exec loader.sce;

// Small test to see if the function is actually working.
str = "my very long string";
assert_checkequal(cppfind(str,"long"), 8);
assert_checkequal(cppfind(str,"very"), 3);
assert_checkequal(cppfind(str,"short"), -1);
// ====================================================================