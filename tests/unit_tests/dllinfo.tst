// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
info = dllinfo(SCI+"\bin\scilex.exe","machine");
assert_checktrue(info(2) <> "x86" | info(2) <> "x64");

info = dllinfo(SCI+"\bin\api_scilab.dll","machine");
assert_checktrue(info(2) <> "x86" | info(2) <> "x64");

info = dllinfo(SCI+"\bin\scilex.exe","exports");
assert_checkequal(size(info), 0);

info = dllinfo(SCI+"\bin\api_scilab.dll","exports");
assert_checkequal(info(1), "api_scilab.dll");
assert_checktrue(size(info(2),"*") >= 300);
assert_checkequal(type(info(2)(1)), 10);

info = dllinfo(SCI+"\bin\scilex.exe","imports");
assert_checktrue(size(info) > 4);

info = dllinfo(SCI+"\bin\api_scilab.dll","imports");
assert_checktrue(size(info) > 4);
// ====================================================================