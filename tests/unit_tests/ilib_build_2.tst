// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
cd(TMPDIR);
mkdir('ilib_build_2');
cd('ilib_build_2');

//Here with give a complete example on adding new primitive to Scilab
//create the procedure files

f1=['void c_sum(double *a,double *b, double *sum)'
    '{*sum=*a + *b;}'];

mputl(f1, 'c_sum.c');

f2=['void c_sub(double *a,double *b, double *sub)'
    '{*sub=*a - *b;}'];
mputl(f2, 'c_sub.c');

//creating the interface file
i=['#include ""api_scilab.h""'
   'extern void c_sum(double *a,double *b, double *sum);'
   'int sci_csum(char *fname, void* pvApiCtx)' 
   '{'
   '  int *piAddr1 = NULL, *piAddr2 = NULL;'
   '  double data1 = 0, data2 = 0;'
   '  double res = 0;'
   '  getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);'
   '  getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);'
   '  getScalarDouble(pvApiCtx, piAddr1, &data1);'
   '  getScalarDouble(pvApiCtx, piAddr2, &data2);'
   '  c_sum(&data1, &data2, &res);'
   '  createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, res);'
   '  AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;'
   '  return 0;'
   '}'];
   
mputl(i, 'sci_csum.c');

//creating the interface file
j=['#include ""api_scilab.h""'
   'extern void c_sub(double *a,double *b, double *sum);'
   'int sci_csub(char *fname, void* pvApiCtx)' 
   '{'
   '  int *piAddr1 = NULL, *piAddr2 = NULL;'
   '  double data1 = 0, data2 = 0;'
   '  double res = 0;'
   '  getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);'
   '  getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);'
   '  getScalarDouble(pvApiCtx, piAddr1, &data1);'
   '  getScalarDouble(pvApiCtx, piAddr2, &data2);'
   '  c_sub(&data1, &data2, &res);'
   '  createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, res);'
   '  AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;'
   '  return 0;'
   '}'];
   
mputl(j, 'sci_csub.c');

//creating the shared library (a gateway, a Makefile and a loader are generated. 
files=['c_sub.c','sci_csub.c','c_sum.c','sci_csum.c'];
ilib_build('foo',['c_sum','sci_csum';'c_sub','sci_csub'],files,[]);

// load the shared library 
exec loader.sce;

assert_checkequal(c_sum(3,5), 8);
assert_checkequal(c_sub(3,5), -2);

// ulink() all libraries
ulink();
