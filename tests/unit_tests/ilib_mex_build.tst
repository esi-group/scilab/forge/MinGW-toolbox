// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
ilib_verbose(0);
// ====================================================================
// <-- CLI SHELL MODE -->
// ====================================================================
cd(TMPDIR);

mputl([
'#include ""mex.h""'
'void mexFunction(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[])'
'{'
'  int *dims = mxGetDimensions(prhs[0]);'
'  sciprint(""%d %d %d\n"",dims[0],dims[1],dims[2]);'
'}'
],'mexfunction16.c');

ilib_mex_build('libmextest',['mexf16','mexfunction16','cmex'],['mexfunction16.c'],[],'','','','');

exec(TMPDIR+'/loader.sce');
mexf16(rand(2,3,2));
// ====================================================================


