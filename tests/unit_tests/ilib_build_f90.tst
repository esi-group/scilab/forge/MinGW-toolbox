// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
sourcecode=['subroutine incrdoublef90(x,y)'
            '  implicit none'
            '  double precision, intent(in) :: x'
            '  double precision, intent(out) :: y'
            '  y=x+1.'
            'end subroutine incrdoublef90'];
cd(TMPDIR);
mputl(sourcecode,'incrdoublef90.f90');
ilib_for_link('incrdoublef90','incrdoublef90.f90',[],'f');
exec('loader.sce');

n = 1;
m = call("incrdoublef90",n,1,"d","out",[1,1],2,"d");
assert_checkequal(m, 2);
n = 2;
m = call("incrdoublef90",n,1,"d","out",[1,1],2,"d");
assert_checkequal(m, 3);
// ====================================================================
