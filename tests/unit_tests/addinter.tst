// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
chdir(TMPDIR); 
mkdir("addinter");
chdir("addinter"); 

src_file = "SCI/modules/dynamic_link/tests/unit_tests/addinter.c";
dst_file = "TMPDIR/addinter/addinter.c";
copyfile(src_file, dst_file);

ilib_build("libintertest", ["scifun1" "intfun1"], "addinter.c", []);
exec loader.sce ;

//check addinter add scifun1
assert_checktrue(funptr("scifun1") <> 0);
assert_checkequal(scifun1(%pi), sin(%pi+1)/%pi);

//remove scifun1
assert_checktrue(clearfun("scifun1"));

//remove dynamic library link
ulink();
