// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
cd(TMPDIR);
mkdir("ilib_build");
cd("ilib_build");

//Here with give a complete example on adding new primitive to Scilab
//create the procedure files
f1=["extern double fun2();"
    "void fun1(double *x, double *y)"
    "{*y=fun2(*x)/(*x);}"];

mputl(f1, "fun1.c");

f2=["#include <math.h>"
    "double fun2(double x)"
    "{ return( sin(x+1.));}"];
    
mputl(f2, "fun2.c");

//creating the interface file
i=["#include ""api_scilab.h"""
   "extern int fun1 ( double *x, double *y);"
   "int intfun1(char* fname, void* pvApiCtx)" 
   "{"
   "  int* piAddr = NULL;"
   "  double data = 0;"
   "  double res = 0;"
   "  getVarAddressFromPosition(pvApiCtx, 1, &piAddr);"
   "  getScalarDouble(pvApiCtx, piAddr, &data);"
   "  fun1(&data, &res);"
   "  createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, res);"
   "  AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;"
   "  return 0;"
   "}"];
mputl(i, "intfun1.c");

//creating the shared library (a gateway, a Makefile and a loader are generated. 
files=["fun1.c","fun2.c","intfun1.c"];
ilib_build("foo",["scifun1","intfun1"],files,[]);

// load the shared library 
exec loader.sce;

//using the new primitive
assert_checkalmostequal(scifun1(33), sin(33+1)/33);

// ulink() all libraries
ulink();
// ====================================================================
