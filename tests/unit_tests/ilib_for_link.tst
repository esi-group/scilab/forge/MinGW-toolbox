// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
cd(TMPDIR);
mkdir("ilib_for_link");
cd("ilib_for_link");

f1=['int ext1c(int *n, double *a, double *b, double *c)'
    '{int k;'
    '  for (k = 0; k < *n; ++k) '
    '      c[k] = a[k] + b[k];'
    '  return(0);}'];

mputl(f1,'fun1.c');

//creating the shared library (a gateway, a Makefile and a loader are generated.
ilib_for_link('ext1c', 'fun1.c', [], "c");

// load the shared library 
exec loader.sce;

a=[1,2,3];
b=[4,5,6];
n=3;

//using the new primitive
c=call('ext1c',n,1,'i',a,2,'d',b,3,'d','out',[1,3],4,'d');
assert_checkequal(c, a + b);

// ulink() all libraries
ulink();
// ====================================================================
