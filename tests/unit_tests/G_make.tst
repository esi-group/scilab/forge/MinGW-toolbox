// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
txt = ["# A simple Makefile",
       "all: message",
       "",
       "message: ",
       ascii(9)+"@echo Hello",
       ""];
       
mputl(txt,"TMPDIR/makefile");
current_dir = pwd();  
cd TMPDIR;
files = G_make(["",""],"message");
cd(current_dir);
// ====================================================================
