// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
create_refs = %f;
// ====================================================================
current_path = pwd(); 
root_path = get_absolute_file_path('tests_mingw.sce'); 
exec (root_path + 'loader.sce'); 
setenv('TOOLBOX_MINGW_PATH', root_path); 
// ====================================================================
if create_refs then
  test_run(root_path, "addinter", "create_ref");
  test_run(root_path, "call", "create_ref");
  test_run(root_path, "dllinfo", "create_ref");
  test_run(root_path, "G_make", "create_ref");
  test_run(root_path, "ilib_build", "create_ref");
  test_run(root_path, "ilib_build_2", "create_ref");
  test_run(root_path, "ilib_build_cpp", "create_ref");
  test_run(root_path, "ilib_build_f90", "create_ref");
  test_run(root_path, "ilib_for_link", "create_ref");
  test_run(root_path, "ilib_for_link_f77", "create_ref");
  test_run(root_path, "ilib_mex_build", "create_ref");
  test_run(root_path, "link", "create_ref");
  test_run(root_path, "links", "create_ref");
  test_run(root_path, "optim_external", "create_ref");
  test_run(root_path, "schur_external", "create_ref");
else
  test_run(root_path);
end
cd(current_path);
// ====================================================================
clear current_path;
clear root_path;
// ====================================================================
