
-- MinGW Compiler Toolbox version 10.3.1
    Update detection of gcc

-- MinGW Compiler Toolbox version 10.3
    Mingw toolbox follow gcc version (10.3)

-- MinGW Compiler Toolbox version 9.2
    Mingw toolbox follow gcc version (9.2)

-- MinGW Compiler Toolbox version 8.3
    Mingw toolbox follow gcc version (8.3)
    Add c++17 CFLAG

-- MinGW Compiler Toolbox version 0.10.0
    - add missing _MSC_VER flag
    - fix mgw_getRootPath
    - Migration to Scilab 6.0.0
    - tested with gcc 6.2.0

-- MinGW Compiler Toolbox version 0.9.3
   Bug in dlwGenerateMakefile.sci fixed (wrong libname).

-- MinGW Compiler Toolbox version 0.9.2
   Migration to Scilab 5.4.1

-- MinGW Compiler Toolbox version 0.9.1
   Xcos compilation fixed with Scilab 5.4.0

-- MinGW Compiler Toolbox version 0.9
   Migration to Scilab 5.4.0

-- MinGW Compiler Toolbox version 0.8
   * ticket 383 fixed - ilib_compile does not work with gfortran

-- MinGW Compiler Toolbox version 0.7
   gcc-4.5.2 was not detected.
   VS libraries conversion failed.
 tested with :
 (* x86) ftp://ftp.equation.com/gcc/gcc-4.5.2-32.exe
 (* x64) ftp://ftp.equation.com/gcc/gcc-4.5.2-64.exe
    
-- MinGW Compiler Toolbox version 0.6
  bug 8221 fixed - MinGW Compiler failed to install properly in Scilab 5.3-beta 
  
-- MinGW Compiler Toolbox version 0.5
 fix compatibility with gcc-4.6
 tested with :
 (* x86) ftp://ftp.equation.com/gcc/gcc-4.5.1-32.exe
 (* x64) ftp://ftp.equation.com/gcc/gcc-4.5.1-64.exe
 
 and 4.6 (WEEKLY SNAPSHOT) 4.6-20101002
 
-- MinGW Compiler Toolbox version 0.4
 bug 7481 fixed 

-- MinGW Compiler Toolbox version 0.3
 Some modifications to work with Scilab-5.3
 it uses new tools of scilab 5.3 to manage compilers on Windows
 Current version support : C, C++ and fortran 77/90
 tested with :
 (* x86) ftp://ftp.equation.com/gcc/gcc-4.5.0-32.exe
 (* x64) ftp://ftp.equation.com/gcc/gcc-4.5.0-64.exe

-- MinGW Compiler Toolbox version 0.2 beta
 Some modifications to work with Scilab-5.2

-- MinGW Compiler Toolbox version 0.1 beta

Allan CORNET - DIGITEO - 2010
Vincent COUVERT - Scilab Enterprises - 2012
